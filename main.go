package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"net/http"
	"strconv"
)


type pemain struct {
	Id 				int
	Nama 			string
	Negara			string
}

type Response struct {
	NextPage int
	Data  []pemain
}

func connect() (*sql.DB, error){
	db, err := sql.Open("sqlite3", "pemainbadmintonDB.db")
	if err != nil {
		return db, err
	}

	return db, err
}

func getPemain(w http.ResponseWriter, r *http.Request){

	db, errdb := connect()
	if errdb != nil {
		http.Error(w, errdb.Error(), http.StatusBadRequest)
		return
	}
	defer db.Close()
	w.Header().Set("Content-Type", "application/json")

	if r.Method == "GET" {
		pages, found := r.URL.Query()["page"]
		search, found := r.URL.Query()["search"]
		var pageNow int
		var pemains []pemain
		if found {
			nowPage, errPages := strconv.Atoi(pages[0])
			pageNow = nowPage
			if errPages != nil {
				http.Error(w, errPages.Error(), http.StatusInternalServerError)
				return
			}
			page := strconv.Itoa((nowPage - 1) * 10)
			rows, errq := db.Query("SELECT * FROM pemain WHERE Nama LIKE '%"+ search[0] +"%' ORDER BY ID ASC " +
				"LIMIT 10 OFFSET " + page)
			if errq != nil {
				http.Error(w, errq.Error(), http.StatusInternalServerError)
				return
			}
			defer rows.Close()

			for rows.Next() {
				var pemain= pemain{}
				var err = rows.Scan(&pemain.Id, &pemain.Nama, &pemain.Negara)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				pemains = append(pemains, pemain)
			}
			var response Response
			response.Data = pemains

			if pemains == nil {
				response.NextPage = 1
			} else {
				response.NextPage = pageNow + 1
			}

			json.NewEncoder(w).Encode(response)
			return
		}
	}

	http.Error(w, "Error, Missing Parameters", http.StatusBadRequest)
}

func main() {
	http.HandleFunc("/pemains", getPemain)

	fmt.Println("Starting Server at http://localhost:2008/")
	http.ListenAndServe(":2008", nil)
}
